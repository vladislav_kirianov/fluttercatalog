import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AuthState extends Equatable {
  AuthState([List props = const []]) : super(props);
}

class Uninitialized extends AuthState {
  @override
  String toString() => 'Uninitialized';
}

class Authenticated extends AuthState {
  final String name;

  Authenticated(this.name) : super([name]);

  @override
  String toString() => 'Authenticated { displayName: $name }';
}

class Unauthenticated extends AuthState {
  @override
  String toString() => 'Unauthenticated';
}
