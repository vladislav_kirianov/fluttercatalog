import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_catalog/home_screen.dart';
import 'package:flutter_catalog/login/login_screen.dart';
import 'package:flutter_catalog/simple_bloc_delegate.dart';
import 'package:flutter_catalog/splash_screen.dart';
import 'package:flutter_catalog/user_repository.dart';

import 'auth/auth_bloc/bloc.dart';

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final UserRepository userRepository = UserRepository();
  runApp(
    BlocProvider(
      builder: (context) => AuthenticationBloc(userRepository: userRepository)
        ..dispatch(AppStarted()),
      child: App(userRepository: userRepository),
    ),
  );
}

class App extends StatelessWidget {
  final UserRepository _userRepository;

  App({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocBuilder(
          bloc: BlocProvider.of<AuthenticationBloc>(context),
          builder: (BuildContext context, AuthState state) {
            if (state is Uninitialized) {
              return SplashScreen();
            } else if (state is Authenticated) {
              return HomeScreen(name: state.name);
            } else if (state is Unauthenticated) {
              return LoginScreen(userRepository: _userRepository);
            } else {
              return Scaffold(
                  appBar: AppBar(
                    title: Text("Catalog"),
                  ),
                  body:
                      Container(child: Center(child: Text("Something else"))));
            }
          }),
      theme: ThemeData(primarySwatch: Colors.indigo),
    );
  }
}
